import React, { Component } from 'react';
import cls from 'classnames'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Menu from 'App/Components/Menu';
import Profile from 'App/Components/Profile';

import { toggleMenu } from 'App/Reducers/Menu/menuActions';

class Nav extends Component {
    constructor(props){
        super(props)
    }
    
    render() {
        return (
            <div id="Nav">
                <nav className="navbar">
                    <button className="menu_button" onClick={this.props.toggleMenu}>
                        <div className={cls("menu_icon", {open : this.props.menu.open})}>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </button>
                    <a href="" className="logo_icon">
                        <img src='/assets/images/logo/logo.svg' alt="Logo" />
                    </a>
                    {!this.props.screen.mobile ? <Menu /> :null}
                    {!this.props.screen.mobile ? <Profile /> :null}
                </nav>
                {this.props.screen.mobile ? <Menu /> :null}
                
            </div>

        );
    }
}

const StateToProps = state => ({ ...state })
const DispatchToProps = dispatch => 
    bindActionCreators({ toggleMenu }, dispatch)
export default connect(StateToProps, DispatchToProps)(Nav)

