import React, { Component } from 'react';
import { connect } from 'react-redux';
import cls from 'classnames'

import Profile from 'App/Components/Profile';

const links = ["Novidades","Inpirações","Duratex","Troque seu Pontos"]

class Menu extends Component {

    render() {
        return (
            <div id="Menu" className={cls("menu", {open: this.props.menu.open})}>
                <div className="menu-wrapper">
                    {this.props.screen.mobile ? <Profile /> :null}
                
                    <ul className="link_list">
                        {links.map((link, i ) => {
                            return (
                                <li className="menu-link" key={i}>
                                    <a href="#">{link}</a>
                                </li>
                            )
                        })}
                    </ul>
                </div>

                <button className="btn_menu-points">Ganhe moedas</button>                
            </div>
        );
    }
}

const StateToProps = state => ({ ...state })
export default connect(StateToProps)(Menu)
