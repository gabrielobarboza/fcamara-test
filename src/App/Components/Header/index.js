import React, { Component } from 'react';

class Header extends Component {
    render() {
        return (
            <div id="Header" className="header-banner">
                <h1 className="header_title">
                    Faça parte<br/> do clube.
                </h1>
                <div className="header_strip"></div>

                <p className="header_text">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vitae turpis elementum, ultrices elit dignissim, gravida augue.
                    <span>Pellentesque laoreet justo ut ipsum dictum pulvinar. Nunc eget</span>
                </p>
            </div>
        );
    }
}

export default Header;