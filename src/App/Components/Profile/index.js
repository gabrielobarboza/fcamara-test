import React, { Component } from 'react';
import cls from 'classnames'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { toggleProfile } from 'App/Reducers/Menu/menuActions';

const wallet = [
    {
        label: "Sua carteira",
        icon: "carteira"
    },
    {
        label: "2000",
        icon: "moeda"
    }
]

const links = [
    {
        label: "Seu perfil",
        icon: "perfil"
    },
    {
        label: "Notas cadastradas",
        icon: "notas"
    },
    {
        label: "Extrato de pontos",
        icon: "extratos"
    },
    {
        label: "Classificação",
        icon: "classificacao"
    },
    {
        label: "Regulamento",
        icon: "regulamentos"
    },
    {
        label: "Dúvidas",
        icon: "duvidas"
    },
    {
        label: "Sair",
        icon: "sair"
    },
]

class Profile extends Component {
    render() {
        return (
            <div id="Profile" className={cls({open: this.props.menu.profile})}>
                <div className="profile_header" onClick={this.props.toggleProfile}>
                    <div className="user_photo">
                        <img src="/assets/images/thumb/user.png" alt="Lorem Ipsum"/>
                    </div>
                    <img className="profile_arrow" src="/assets/images/icons/baixo.svg" />
                    <span className="user_name">Lorem Ipsum</span>
                </div>
                <div className="profile_items">
                    <ul className="profile_wallet">
                        {wallet.map((item, i)=> {
                            return (
                                <li className={`item-${item.icon}`} key={i}>
                                    <a href="#">
                                        <i className="icon"><img src={`/assets/images/icons/${item.icon}.svg`} alt={item.label}/></i>
                                        <span>{item.label}</span>
                                    </a>
                                </li>
                            )
                        })}
                      
                    </ul>
                    <ul className="profile_links">
                        {links.map((link, i)=> {
                            return (
                                <li className={`item-${link.icon}`} key={i}>
                                    <a href="#">
                                        <i className="icon"><img src={`/assets/images/icons/${link.icon}.svg`} alt={link.label}/></i>
                                        <span>{link.label}</span>
                                    </a>
                                </li>
                            )
                        })}
                    </ul>
                </div>
            </div>
        );
    }
}

const StateToProps = state => ({ ...state })
const DispatchToProps = dispatch => 
    bindActionCreators({ toggleProfile }, dispatch)
export default connect(StateToProps, DispatchToProps)(Profile)
