import React, { Component } from 'react';

class Box extends Component {
    render() {
        return (
            <div className="article_item">
                <div className="article_photo"><img src={`/assets/images/articles/${this.props.img}.png`} /></div>
                <h4 className="article_title">Lorem ipsum</h4>
                <p className="article_description">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu ex ullamcorper, mattis ipsum facilisis, eleifend purus. Suspendisse malesuada sed ex vel cursus. Proin augue massa, lacinia quis magna non, ultrices vehicula mi. Ut vulputate tellus in egestas fermentum. Maecenas arcu ante, imperdiet sed ullamcorper at, rhoncus quis neque. Phasellus leo odio, malesuada sed mi sit amet, blandit ullamcorper tellus. Integer quis tortor tincidunt, mattis mauris nec, molestie sapien. Pellentesque eu maximus mi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris gravida accumsan augue a sollicitudin. Cras non feugiat diam.
                </p>
            </div>
        );
    }
}

export default Box;
