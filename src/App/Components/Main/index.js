import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {setScreen} from 'App/Reducers/Screen/screenActions'

import Nav from 'App/Components/Nav';
import Header from 'App/Components/Header';
import List from 'App/Components/List';

const screenProps = () => ({
    landscape: window.innerWidth >= window.innerHeight,
    width: window.innerWidth,
    height: window.innerHeight
})

const destaques = [16,15,13,6]
const inspiracao = [14,12,11,5]
const duratex = [10,9,8,7]
const marcenaria = [4,3,2,1]


class Main extends Component {
    constructor(props) {
        super(props);

        this.updateDimensions =this.updateDimensions.bind(this)
    }

    componentWillMount() {
        this.updateDimensions()
        window.addEventListener("resize", this.updateDimensions, false);        
    }

    componentDidMount() {
        this.setState({ mounted: true})
    }

    updateDimensions () {
        this.props.setScreen(screenProps())
        this.forceUpdate()
    }
    
    render() {
        return (
            <div id="Main">
                <Nav />
                <div className="wrapper">
                    <Header />
                    <List title="Destaques" items={destaques}/>
                    <List title="Inspirações" items={inspiracao}/>
                    <List title="Noticias da Duratex" items={duratex}/>
                    <List title="Para a sua marcenaria" items={marcenaria}/>
                </div>
            </div>
        )
    }
}

const StateToProps = state => ({ ...state })
const DispatchToProps = dispatch => 
    bindActionCreators({ setScreen }, dispatch)
export default connect(StateToProps, DispatchToProps)(Main)

