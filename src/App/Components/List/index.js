import React, { Component } from 'react';
import Slider from "react-slick";
import { connect } from 'react-redux';

import Article from 'App/Components/Article';

function NextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div className={className}
            style={{ ...style }}
            onClick={onClick}>
            <svg x="0px" y="0px" width="50px" height="50px" viewBox="0 0 50 50" enableBackground="new 0 0 50 50" >
                    <path id="Path_26" fill="#FFFFFF" d="M40.90909,25L15.64091,50l-6.55-6.48182L27.80909,25L9.09091,6.48182L15.64091,0L40.90909,25z"
                />
            </svg>
        </div>
    );
}
  
function PrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div className={className}
        style={{ ...style }}
        onClick={onClick}>
            <svg x="0px" y="0px" width="50px" height="50px" viewBox="0 0 50 50" enableBackground="new 0 0 50 50">
                <path id="Path_26" fill="#FFFFFF" d="M9.0909,25.00001l25.26818-25l6.55,6.48182L22.1909,25.00001l18.71819,18.51818l-6.55,6.48182L9.0909,25.00001z"/>
            </svg>
        </div>
    );
}

class List extends Component {
    componentWillMount() {
        let list = this
        window.addEventListener("resize", list.forceUpdate, false);        
    }

    render() {
        const settings = {
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: this.props.screen.mobile ? 1 : 3,
            slidesToScroll: 1,
            nextArrow: <NextArrow />,
            prevArrow: <PrevArrow />
        };

        return (
            <div className="articles_list" >
                <h3 className="list_title">{this.props.title}</h3>
                <Slider {...settings}  ref={slider => this.slider = slider}>
                    {this.props.items.map((img, i) => <Article img={img} key={i}/>)}
                </Slider>
            </div>
        );
    }
}

const StateToProps = state => ({ ...state })
export default connect(StateToProps)(List)
