import vars from 'Assets/styles/_vars.scss'

const SCREEN_INITIAL_STATE = { landscape: window.innerWidth >= window.innerHeight, width: window.innerWidth, height: window.innerHeight, mobile: false }

export default (state = SCREEN_INITIAL_STATE, action) => {
    switch(action.type) {
        case 'SET_SCREEN':
            let { props , value } = action
            if(props instanceof Object && !Array.isArray(props)) {
                for (var i in props) {
                    state[i] = props[i]
                }
            } else if (typeof(props) === 'string') {
                state[props] = value
            }

            state.mobile = state.width <= parseInt(vars.mobile)
       
            return state
        default:
            return state
    }
}