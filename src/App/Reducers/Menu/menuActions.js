export const toggleMenu = (open) => {

    return dispatch => {
        dispatch({ type: 'TOGGLE_MENU', open })
    }
}

export const toggleProfile = (open) => {
    return dispatch => {
        dispatch({ type: 'TOGGLE_PROFILE', open })
    }
}