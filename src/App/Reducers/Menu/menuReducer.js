const MENU_INITIAL_STATE = { open: false, profile: false }

export default (state = MENU_INITIAL_STATE, action) => {
    switch(action.type) {
        case 'TOGGLE_MENU':
            let {open} = state

            if(action.open === true || action.open === false) {
                state.open = action.open
            } else {
                state.open = !open
            }

            return {...state}
        case 'TOGGLE_PROFILE':
            let {profile} = state

            if(action.open === true || action.open === false) {
                state.profile = action.open
            } else {
                state.profile = !profile
            }

            return {...state}
        default:
            return state
    }
}
