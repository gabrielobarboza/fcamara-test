import menuReducer from './Menu/menuReducer';
import screenReducer from './Screen/screenReducer';

import { combineReducers } from 'redux'

const rootReducer = combineReducers({
    menu : menuReducer,
    screen: screenReducer
})

export default rootReducer